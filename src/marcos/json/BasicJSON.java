package marcos.json;

import com.google.gson.Gson;

public class BasicJSON {

    static class Pet{
        String name;
        String age;
        String breed;
        public Pet(String name, String age, String breed){
            this.name = name;
            this.age = age;
            this.breed = breed;
        }

    }
    public static void main(String[] args) {
        // To JSON
        Pet pet = new Pet("Jack", "7", "Beagle");

        Gson gson = new Gson();

        String json = gson.toJson(pet);

        System.out.println(json);

        // To object
        String jsonText = "{\"name\":\"Jack\",\"age\":\"7\",\"breed\":\"Beagle\"}";

        Pet pet1 = gson.fromJson(jsonText,Pet.class);

        System.out.println("Name:" + pet.name);
        System.out.println("Age:" + pet.age);
        System.out.println("Breed:" + pet.breed);

    }
}
